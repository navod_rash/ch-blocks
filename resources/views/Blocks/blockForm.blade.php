@extends('Admin.layout')

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/summernote/summernote-lite.css") }}">
@endsection
@section('content')
  <div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
      <h3 class="content-header-title mb-0">@lang('blocks.title')</h3>
      <div class="row breadcrumbs-top">
        <div class="breadcrumb-wrapper col-12">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('content.pageNames.dashboard')</a>
            </li>
            <li class="breadcrumb-item"><a href="{{ route('admin.blocks') }}">@lang('blocks.folder.title')</a>
            </li>
            <li class="breadcrumb-item active">{{ $folder->folder }}
            </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content-body">
    <section id="ajax">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title" id="basic-layout-form">@lang('blocks.block.creation.createBlockTitle')</h4>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">

                <form class="form" id="form" method="post" action="{{ route('admin.SaveBlock') }}">
                  @csrf
                  <input type="hidden" name="folderID" value="{{ $folder->id }}">
                  <div class="form-body">
                    @include('Admin.partials.multi-language-select-locale')
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="projectinput1">@lang('blocks.block.creation.createBlockHeading')</label>
                          <input
                            type="text"
                            class="form-control always-show-maxlength block-heading"
                            id="maxlength-always-show"
                            placeholder="@lang('blocks.block.creation.createBlockHeadingPlaceholder')"
                            maxlength="100"
                            name="title"
                          />
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="projectinput1">@lang('blocks.block.creation.createBlockSystemName')</label>
                          <input type="text" class="form-control block-system-name" disabled readonly/>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="summernote">@lang('blocks.block.creation.createBlockContent')</label>
                          <textarea id="summernote" class="form-control" name="html"></textarea>
                          {{--<input type="hidden" id="html" name="html">--}}
                        </div>
                      </div>
                    </div>
                    @if(Plugin::isActive('Gallery'))
                      {!! Gallery::RenderChooseButton() !!}
                    @endif
                  </div>
                  <div class="form-actions">
                    <a href="{{ URL::previous() }}" class="btn btn-outline-light mr-1">
                      <i class="ft-x"></i> @lang('blocks.back')
                    </a>
                    <button type="submit" class="btn btn-primary">
                      <i class="fa fa-check-square-o"></i> @lang('blocks.block.creation.createButton')
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

@endsection

@section("scripts")
  <script src="{{ asset("vendors/js/forms/extended/maxlength/bootstrap-maxlength.js") }}"
          type="text/javascript"></script>
  <script src="{{ asset("js/scripts/forms/extended/form-maxlength.js") }}" type="text/javascript"></script>
  <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
  <script src="{{ asset("js/scripts/summernote/summernote-lite.js") }}" type="text/javascript"></script>
  <script src="{{ asset("js/scripts/summernote/lang/summernote-cs-CZ.min.js") }}"
          type="text/javascript"></script>
  <script src="{{ asset("js/scripts/functions.js") }}" type="text/javascript"></script>

  {{--<script>--}}
    {{--$('#summernote').summernote({--}}
      {{--lang: 'cs-CZ',--}}
      {{--height: 300--}}
    {{--});--}}

    {{--$("#form").on("submit", function (e) {--}}
      {{--let markupStr = $('#summernote').summernote('code');--}}
      {{--$("#html").val(markupStr);--}}
    {{--});--}}
  {{--</script>--}}
@endsection
