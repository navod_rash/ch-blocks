<?php

namespace Creativehandles\ChBlocks;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\ChBlocks\Skeleton\SkeletonClass
 */
class ChBlocksFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-blocks';
    }
}
