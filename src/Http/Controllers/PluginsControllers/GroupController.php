<?php

namespace Creativehandles\ChBlocks\Http\Controllers\PluginsControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Creativehandles\ChBlocks\Plugins\Groups\Groups as Groups;

class GroupController extends Controller
{
    protected $groups;

    public function __construct()
    {

        // Assign blocks plugin into variable
        $this->groups = new Groups();
    }

    /**
     * Creates group for folders
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function CreateGroup(Request $request)
    {
        if($request->has('group')) {
            $group = $this->groups->createGroup($request->group);

            return back()->with("createdGroup", $group ? true : false);
        }
    }

  /**
   * Assign folder to the group
   *
   * @param Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function AssignGroup(Request $request)
  {
    $this->groups->assignGroup($request->groupID, $request->folderID);

    return back()->with("assigned", true);
  }

  public function SaveFoldersPosition(Request $request)
  {
    if($this->groups->savePosition($request->array)) {
      return response()->json(['status' => true]);
    }

    return response()->json(['status' => false]);
  }

}
