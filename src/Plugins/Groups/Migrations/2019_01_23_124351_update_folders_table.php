<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('blocks_folder', function($table) {
        $table->integer('group')->nullable();
        $table->integer('position')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('blocks_folder', function($table) {
        $table->dropColumn('group');
        $table->dropColumn('position');
      });
    }
}
