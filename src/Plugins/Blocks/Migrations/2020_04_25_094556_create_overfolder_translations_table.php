<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOverfolderTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overfolder_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('overfolder_id');
            $table->char('locale', 2)->index();
            $table->string('folder', 200)->default(null)->nullable();
            $table->timestamps();

            $table->unique(['overfolder_id', 'locale'], 'overfolder_id_locale');
            $table->foreign('overfolder_id', 'fk_overfolder_id')->references('id')->on('overfolders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overfolder_translations');
    }
}
