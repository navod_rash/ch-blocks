<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TransferDataToOverfolderTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // move the existing data into the translations table
        DB::statement(
            'insert into overfolder_translations (overfolder_id, locale, folder) select id, :locale, folder from overfolders',
            ['locale' => config('app.locale')])
        ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Move the translated data back into the main table. Translations of the default language of the application are selected.
        DB::statement(
            'update overfolders as m join overfolder_translations as t on m.id = t.overfolder_id set m.folder = t.folder where t.locale = :locale',
            ['locale' => config('app.locale')])
        ;
    }
}
