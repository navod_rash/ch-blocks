<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create the translations table
        if (! Schema::hasTable('block_translations')) {
            Schema::create('block_translations', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('block_id');
                $table->string('locale', 10)->index();
                $table->string('name', 100)->default(null)->nullable();
                $table->binary('content')->default(null)->nullable();
                $table->string('image', 500)->default(null)->nullable();
                $table->timestamps();

                $table->unique(['block_id', 'locale']);
                $table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('block_translations');
    }
}
