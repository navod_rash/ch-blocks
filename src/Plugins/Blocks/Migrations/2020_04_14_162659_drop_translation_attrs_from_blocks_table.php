<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTranslationAttrsFromBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // we drop the translation attributes in our main table:
        Schema::table('blocks', function ($table) {
            $table->dropColumn('name');
            $table->dropColumn('content');
            $table->dropColumn('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // add the translatable attributes back in to main table
        Schema::table('blocks', function ($table) {
            $table->string('name', 100)->default(null)->nullable()->after('folder');
            $table->binary('content')->default(null)->nullable()->after('name');
            $table->string('image', 500)->default(null)->nullable()->after('content');
        });
    }
}
