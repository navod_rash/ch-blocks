<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferDataToBlockTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // move the existing data into the translations table
        DB::statement(
            'insert into block_translations (block_id, locale, name, content, image) select id, :locale, name, content, image from blocks',
            ['locale' => config('app.locale')])
        ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Move the translated data back into the main table. Translations of the default language of the application are selected.
        DB::statement(
            'update blocks as m join block_translations as t on m.id = t.block_id set m.name = t.name, m.content = t.content, m.image = t.image where t.locale = :locale',
            ['locale' => config('app.locale')])
        ;
    }
}
